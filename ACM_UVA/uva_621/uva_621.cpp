
#include<stdio.h>
#include<string.h>
int main()
{
	freopen("inp.in", "r", stdin);
	int T, len = 0;scanf("%d%*c", &T);
	char s[200];
	while (T--)
	{
		scanf("%s%*c", s);
		len = strlen(s);
		if (len >= 2 && s[len - 2] == '3' && s[len - 1] == '5')printf("-\n");
		else if (s[0] == '9' && s[len - 1] == '4')printf("*\n");
		else if (len >= 3 && s[0] == '1' && s[1] == '9' && s[2] == '0')printf("?\n");
		else printf("+\n");
	}
	return 0;
}
