
//https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=208
#include<stdio.h>
#include<iostream>
#include<string>
using namespace std;
int main()
{
freopen("inp.in" , "r" , stdin);
int count = 0;
string s = "";
while(!cin.eof())
{
getline(cin , s);
if(count)cout<<endl;
for (int i = 0; i < s.size(); i++)
{
if(s[i] == '"')
if(count % 2 == 0 )s.replace(i , 1 , "``") , count++;
else s.replace(i , 1 , "''") , count++;
}
cout<<s;
}
return 0;
}

