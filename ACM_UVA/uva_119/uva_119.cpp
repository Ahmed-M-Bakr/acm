
#include<stdio.h>
#include<string.h>
int findIdx(char *s, char names[][80], int size)
{
	for (int i = 0; i < size; i++)
	{
		if (!strcmp(s, names[i]))return i;
	}
	return 0;
}
int main()
{
	freopen("inp.in", "r", stdin);
	int N, n, money, t = 0;
	char names[11][80];
	int arr[11];
	while (scanf("%d%*c", &N) == 1)
	{
		if (t++)printf("\n");
		for (int i = 0; i < N; i++) { scanf("%s%*c", &names[i]);arr[i] = 0; }

		for (int i = 0; i < N; i++)
		{
			char name[80];
			scanf("%s%*c", &name);
			scanf("%d%d", &money, &n);
			if (!n)continue;
			arr[findIdx(name, names, N)] -= money - (money % n);

			for (int j = 0; j < n; j++)
			{
				scanf(" %s ", &name);
				arr[findIdx(name, names, N)] += money / n;
			}
		}

		for (int i = 0; i < N; i++)
		{

			printf("%s %d\n", names[i], arr[i]);
		}

	}
	return 0;
}
