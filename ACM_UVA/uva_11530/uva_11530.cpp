//AC
#include<stdio.h>
#include<string.h>
int arr[] = { 1 , 2 , 3 , 1 , 2 , 3 , 1 , 2 , 3 , 1 , 2 , 3 , 1 , 2 , 3 , 1 , 2,  3 , 4 , 1 , 2 , 3 , 1 , 2 , 3 , 4 };
int main() {
	freopen("inp.in", "r", stdin);
	int T, c;
	char s[200], *p;
	scanf("%d%*c", &T);

	for (int t = 1; t <= T; t++)
	{
		gets(s);
		c = 0;
		p = s;
		while (*p) {
			if (*p == ' ')c++;
			else c += arr[*p - 'a'];
			p++;
		}
		printf("Case #%d: %d\n", t, c);
	}
	return 0;
}
