
#include<stdio.h>
int main()
{
	freopen("inp.in", "r", stdin);
	int T, l, w, h, c = 1;scanf("%d", &T);
	while (T--)
	{
		scanf("%d %d %d", &l, &w, &h);
		if (l > 20 || w > 20 || h > 20)printf("Case %d: bad\n", c++);
		else printf("Case %d: good\n", c++);
	}
	return 0;
}
