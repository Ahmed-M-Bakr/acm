
//https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=3565
#include<stdio.h>
#include<iostream>
#include<string>
using namespace std;
int main()
{
	freopen("inp.in", "r", stdin);
	string s;
	int c = 0;
	while (!cin.eof())
	{
		getline(cin, s);
		if (c++)cout << endl;
		cout << s;
	}
	return 0;
}
