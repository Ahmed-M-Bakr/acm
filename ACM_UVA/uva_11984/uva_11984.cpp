//AC
#include<stdio.h>

float c_to_f(float c) {
	return (9.0 / 5) * c + 32;
}

float f_to_c(float f) {
	return (f - 32) * (5.0 / 9);
}

int main() {
	freopen("inp.in", "r", stdin);
	int T, c, incF;
	scanf("%d", &T);
	for (int t = 1; t <= T; t++)
	{
		scanf("%d %d", &c, &incF);
		printf("Case %d: %.02f\n", t, f_to_c(c_to_f(c) + incF));
	}
	return 0;
}
