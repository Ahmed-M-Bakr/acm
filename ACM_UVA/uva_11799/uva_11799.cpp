
#include<stdio.h>
int main()
{
	freopen("inp.in", "r", stdin);
	int T, max, n, num;scanf("%d", &T);
	for (int t = 1; t <= T; t++)
	{
		max = 0;
		scanf("%d", &n);
		while (n--)
		{
			scanf("%d", &num);
			if (max < num)max = num;
		}
		printf("Case %d: %d\n", t, max);
	}
	return 0;
}
