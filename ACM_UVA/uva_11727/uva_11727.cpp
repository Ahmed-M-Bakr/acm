
#include<stdio.h>
#include<algorithm>
using namespace std;
int main()
{
	freopen("inp.in", "r", stdin);
	int T, x[3], c = 1;scanf("%d", &T);
	while (T--)
	{
		scanf("%d %d %d", &x[0], &x[1], &x[2]);
		sort(x, x + 3);
		printf("Case %d: %d\n", c++, x[1]);
	}
	return 0;
}
