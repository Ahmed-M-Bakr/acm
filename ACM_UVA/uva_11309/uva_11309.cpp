//AC
#include<stdio.h>
#include<string.h>
int h, m, num;
bool isPal(const char * s) {
	int len = strlen(s);
	for (int i = 0; i < len / 2; i++)
		if (s[i] != s[len - i - 1])return false;
	return true;
}
int main() {
	freopen("inp.in", "r", stdin);
	int T;
	char arr[5];
	scanf("%d", &T);
	while (T--) {
		scanf("%d:%d", &h, &m);
		while (1) {
			m++;
			if (m > 59) {
				m = 0;
				h++;
				if (h > 23)h = 0;
			}
			//convert h and m to num
			num = h * 100 + m;
			sprintf(arr, "%d", num);//convert num to string
			if (isPal(arr)) {
				printf("%02d:%02d\n", h, m);
				break;
			}
		}
	}
	return 0;
}
