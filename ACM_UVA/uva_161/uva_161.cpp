//AC
#include<stdio.h>
#include<algorithm>
using namespace std;
int inp[110], inpSize, arr[18001], minn;//18001sec = 5hours * 60 * 60
bool takeInput(void) {
	scanf(" %d %d %d ", &inp[0], &inp[1], &inp[2]);
	if (!inp[0] && !inp[1] && !inp[2])return false;
	if (!inp[2]) {
		inpSize = 2;
		return true;
	}
	inpSize = 3;
	for (inpSize = 3;scanf("%d", &inp[inpSize]), inp[inpSize];inpSize++);
	int x = 2;
	return true;
}

void process(int n) {
	int greenPeriod = n - 5;
	int notGreenPeriod = n + 5;
	int idx = 0, start, end;
	while (idx < 18001) {
		start = idx;
		end = idx + greenPeriod;
		for (idx = start; idx < end && idx < 18001; idx++)
		{
			arr[idx]++;
		}
		idx += notGreenPeriod;
	}
}

int main() {
	freopen("inp.in", "r", stdin);
	//freopen("output" , "w" , stdout);
	int c, hr, minute, sec;
	while (1) {//begin of new case
		if (!takeInput())break;
		for (int i = 0; i < 18001; i++)arr[i] = 0;
		for (int i = 0; i < inpSize; i++)
		{
			process(inp[i]);
		}
		int i = 0;
		while (arr[i++] == inpSize);
		for (; i < 18001 && arr[i] != inpSize; i++);
		minute = i / 60;
		hr = minute / 60;

		if (i < 18001)
			printf("%02d:%02d:%02d\n", hr % 60, minute % 60, i % 60);
		else
			printf("Signals fail to synchronise in 5 hours\n");
	}
	return 0;
}
