
#include<stdio.h>
int main()
{
	freopen("inp.in", "r", stdin);
	int numPeople, budget, numHotels, numWeekend;
	int minbudget, availableBeds, hotelPrice;
	while (scanf("%d %d %d %d ", &numPeople, &budget, &numHotels, &numWeekend) == 4)
	{
		minbudget = 9999999;
		for (int i = 0; i < numHotels; i++)
		{
			scanf("%d", &hotelPrice);
			bool isSuitable = false;
			for (int j = 0; j < numWeekend; j++)
			{
				scanf("%d", &availableBeds);
				if (availableBeds >= numPeople)isSuitable = true;
			}
			if (isSuitable && minbudget > hotelPrice)minbudget = hotelPrice;
		}
		if (minbudget * numPeople <= budget)printf("%d\n", minbudget * numPeople);
		else printf("stay home\n");
	}
	return 0;
}
