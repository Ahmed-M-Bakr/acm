#include<stdio.h>
#include<string>
#include<string.h>
#include<vector>
#include<algorithm>
using namespace std;

char inp[10000];
char out[10000];
bool visited[10000];
int len;
vector<string>vec;
void rec(int size) {
	if (size == len) {
		vec.push_back(string(out));
		return;
	}
	for (int i = 0; i < len; i++)
	{
		if (visited[i])
			continue;
		visited[i] = true;
		out[size] = inp[i];
		rec(size + 1);
		visited[i] = false;
	}
}
int main() {
	freopen("inp.in", "r", stdin);
	//freopen("output" , "w" , stdout);
	int T;scanf("%d", &T);
	while (T--) {
		scanf(" %s ", inp);
		len = strlen(inp);
		out[len] = '\0';
		vec.clear();
		rec(0);
		sort(vec.begin(), vec.end());
		string out = "";
		for (int i = 0; i < vec.size(); i++)
			out += vec[i] + "\n";
		printf("%s", out.c_str());

	}
	return 0;
}