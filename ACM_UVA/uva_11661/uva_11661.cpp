
#include<stdio.h>
int main()
{
	freopen("inp.in", "r", stdin);
	int len, min, r, d, i;
	char c;
	while (1)
	{
		scanf("%d%*c", &len);
		if (!len)break;
		min = 3000000;
		r = d = -3000000;
		i = 0;
		while (len--)
		{
			scanf("%c", &c);
			if (c == 'Z')min = 0;
			else if (c == 'R')
			{
				r = i;
				if (min > r - d)min = r - d;
			}
			else if (c == 'D')
			{
				d = i;
				if (min > d - r)min = d - r;
			}

			i++;
		}

		printf("%d\n", min);
	}
	return 0;
}
