
#include<stdio.h>
#include<string.h>
int main()
{
	freopen("inp.in", "r", stdin);
	int T, account = 0, k;scanf("%d%*c", &T);
	char inp[80];
	while (T--)
	{
		scanf("%s%*c", inp);
		if (!strcmp("donate", inp))
		{
			scanf("%d%*c", &k);
			account += k;
		}
		else printf("%d\n", account);
	}
	return 0;
}
