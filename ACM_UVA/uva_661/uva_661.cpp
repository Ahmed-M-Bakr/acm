
#include<stdio.h>
unsigned long long devices[21];
bool state[21];
int main()
{
	freopen("inp.in", "r", stdin);
	unsigned long long n, m, c, temp, sum, max, seq = 0;
	while (scanf("%llu%llu%llu", &n, &m, &c) == 3 && n && m && c)
	{
		printf("Sequence %llu\n", ++seq);
		for (int i = 1; i <= n; i++) { scanf("%llu", &devices[i]);state[i] = false; }
		sum = max = 0;
		for (int i = 0; i < m; i++)
		{
			scanf("%llu", &temp);
			state[temp] = !state[temp];
			if (state[temp])sum += devices[temp];
			else sum -= devices[temp];
			if (max < sum)max = sum;
		}

		if (max > c)printf("Fuse was blown.\n\n");
		else printf("Fuse was not blown.\nMaximal power consumption was %llu amperes.\n\n", max);
	}
	return 0;
}
