
#include<stdio.h>
#include<stdlib.h>
#include<string>

using namespace std;
#define ARR_LEN 1081

int main() {
	freopen("inp.in", "r", stdin);
	freopen("output", "w", stdout);
	int n, start, end, t = 1, max, c, candStart;
	char s[500], h[3], m[3];
	bool arr[ARR_LEN + 1];
	arr[599] = arr[ARR_LEN] = true;
	h[2] = m[2] = '\0';
	while (scanf("%d%*c", &n, h) == 1) {
		if (t == 15)
			t = 15;
		for (int i = 600;i < ARR_LEN;i++)arr[i] = false;
		while (n--) {
			gets(s);
			h[0] = s[0];  h[1] = s[1];
			m[0] = s[3];  m[1] = s[4];
			start = atoi(h) * 60 + atoi(m);

			h[0] = s[6];  h[1] = s[7];
			m[0] = s[9];  m[1] = s[10];
			end = atoi(h) * 60 + atoi(m);

			for (int i = start; i <= end; i++)
			{
				arr[i] = true;
			}
		}
		start = candStart = 600;
		max = 0;
		for (int i = 600; i <= ARR_LEN; i++)
		{
			if (arr[i]) {
				if (i == ARR_LEN) {
					if (max < (i - start - 1)) {
						max = (i - start - 1);
						candStart = start;
					}
				}
				else if (max < (i - start) && !arr[i - 1]) {
					max = (i - start);
					if (i == ARR_LEN)
						max--;
					candStart = start;
				}
				start = i;
			}

		}
		if (max == 0)max = 1;
		if (max / 60 == 0)
			printf("Day #%d: the longest nap starts at %02d:%02d and will last for %d minutes.\n", t++, candStart / 60, candStart % 60, max % 60);
		else
			printf("Day #%d: the longest nap starts at %02d:%02d and will last for %d hours and %d minutes.\n", t++, candStart / 60, candStart % 60, max / 60, max % 60);

	}
	return 0;
}
