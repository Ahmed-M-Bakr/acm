
#include<stdio.h>

int main()
{
	freopen("inp.in", "r", stdin);
	int a, b;
	while (scanf("%d %d ", &a, &b) && a != -1)
	{
		int val1 = a - b;
		if (val1 < 0)val1 += 100;
		int val2 = b - a;
		if (val2 < 0)val2 += 100;
		printf("%d\n", val1 < val2 ? val1 : val2);
	}
	return 0;
}
