
#include<stdio.h>
int main()
{
	freopen("inp.in", "r", stdin);
	int init = 5;
	int c[3];
	while (scanf("%d %d %d %d", &init, &c[0], &c[1], &c[2]) && (init || c[0] || c[1] || c[2])) {
		c[2] = c[2] - c[1];
		if (c[2] >= 40)c[2] -= 40;
		if (c[2] < 0)c[2] += 40;
		c[1] = c[1] - c[0];
		if (c[1] >= 40)c[1] -= 40;
		if (c[1] < 0)c[1] += 40;
		c[0] = c[0] - init;
		if (c[0] < 0)c[0] += 40;
		if (c[0] >= 40)c[0] -= 40;
		int s = 40;
		printf("%.0f\n", 360 + 360 + (s - c[0])* (360.0 / 40) + 360 + (c[1]) * (360.0 / 40) + (s - c[2])* (360.0 / 40));
	}
	return 0;
}
