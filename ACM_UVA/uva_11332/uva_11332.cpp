
#include<stdio.h>
int sumDigits(int n)
{
	int sum = 0;
	while (n > 0)
	{
		sum += n % 10;
		n /= 10;
	}
	return sum;
}
int main()
{
	freopen("inp.in", "r", stdin);
	int n;
	while (true)
	{
		scanf("%d", &n);
		if (!n)break;
		while (n / 10)
			n = sumDigits(n);
		printf("%d\n", n);
	}
	return 0;
}
