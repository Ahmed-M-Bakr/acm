
#include<stdio.h>
#include<string.h>
int main()
{
	freopen("inp.in", "r", stdin);
	int T, south, north, w, diff;scanf("%d", &T);
	char res[80];
	while (T--)
	{
		strcpy(res, "yes");
		diff = -1;
		scanf("%d", &w);
		while (w--)
		{
			scanf("%d %d", &south, &north);
			south += 100;
			north += 100;
			if (diff == -1)
			{
				diff = south - north;
			}
			else if (diff != south - north)strcpy(res, "no");
		}
		printf("%s\n", res);
		if (T)printf("\n");

	}
	return 0;
}
