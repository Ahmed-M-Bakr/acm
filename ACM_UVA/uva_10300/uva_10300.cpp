
#include<stdio.h>

int main()
{
	freopen("inp.in", "r", stdin);
	int T, num, a, b;scanf("%d ", &T);
	unsigned long long int sum;
	while (T--)
	{
		scanf("%d", &num);
		sum = 0;
		while (num--)
		{
			scanf("%d%*d%d", &a, &b);
			sum += (a * b);
		}
		printf("%llu\n", sum);
	}
	return 0;
}
