//AC
#include<stdio.h>
#include<string.h>
#include<map>
using namespace std;

map<char, char> mirror;

bool isPal(char * str) {
	int len = strlen(str);
	for (int i = 0; i < len / 2; i++)
	{
		if (str[i] != str[len - i - 1])return false;
	}
	return true;
}

void initMirrorMap() {
	mirror['A'] = 'A';
	mirror['E'] = '3';
	mirror['H'] = 'H';
	mirror['I'] = 'I';
	mirror['J'] = 'L';
	mirror['L'] = 'J';
	mirror['M'] = 'M';
	mirror['O'] = 'O';
	mirror['S'] = '2';
	mirror['T'] = 'T';
	mirror['U'] = 'U';
	mirror['V'] = 'V';
	mirror['W'] = 'W';
	mirror['X'] = 'X';
	mirror['Y'] = 'Y';
	mirror['Z'] = '5';
	mirror['1'] = '1';
	mirror['2'] = 'S';
	mirror['3'] = 'E';
	mirror['5'] = 'Z';
	mirror['8'] = '8';
}

bool isMirror(char * str) {
	int len = strlen(str);
	for (int i = 0; i < len; i++)
	{
		if (mirror.find(str[i]) == mirror.end()) {
			return false;
		}
		if (mirror.find(str[len - i - 1]) == mirror.end()) {
			return false;
		}
		if (mirror[str[i]] != str[len - i - 1]) {
			return false;
		}
	}
	return true;
}

int main() {
	freopen("inp.in", "r", stdin);
	initMirrorMap();
	char str[1000];
	char * ptr = gets(str);
	while (ptr != NULL) {
		if (isMirror(str) && isPal(str)) {
			printf("%s -- is a mirrored palindrome.\n\n", str);
		}
		else if (isMirror(str)) {
			printf("%s -- is a mirrored string.\n\n", str);
		}
		else if (isPal(str)) {
			printf("%s -- is a regular palindrome.\n\n", str);
		}
		else {
			printf("%s -- is not a palindrome.\n\n", str);
		}
		ptr = gets(str);
	}
	return 0;
}
