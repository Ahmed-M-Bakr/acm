
#include<stdio.h>

int courses[101];
int numFCourses, numCategories, numCoursesInCat, minCoursesInCat;
int isInArr(int courseNum)
{
	for (int i = 0; i < numFCourses; i++)
		if (courses[i] == courseNum)
			return 1;
	return 0;
}
int main()
{
	freopen("inp.in", "r", stdin);
	while (scanf("%d %d", &numFCourses, &numCategories) == 2)
	{
		for (int i = 0; i < numFCourses; i++)scanf("%d", &courses[i]);
		int numMetRequirements = 0;
		for (int j = 0; j < numCategories; j++)
		{
			scanf("%d %d", &numCoursesInCat, &minCoursesInCat);
			int count = 0, courseNum;
			for (int i = 0; i < numCoursesInCat; i++)
			{
				scanf("%d", &courseNum);
				count += isInArr(courseNum);
			}
			if (count >= minCoursesInCat)numMetRequirements++;

		}
		if (numMetRequirements == numCategories)printf("yes\n");
		else printf("no\n");
	}
	return 0;
}
