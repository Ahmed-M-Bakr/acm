//AC
#include<stdio.h>
#include<string.h>
#include<math.h>
char inp[11000], s[11000], **grid;
int sLen, gridSize;
void processInput() {
	int inpIdx = 0, sIdx = 0;
	while (inp[inpIdx]) {
		if (inp[inpIdx] >= 'a' && inp[inpIdx] <= 'z')
			s[sIdx++] = inp[inpIdx];
		inpIdx++;
	}
	s[sIdx] = '\0';
}

void arrangeToGrid() {
	grid = new char*[gridSize];
	int k = 0;
	for (int i = 0; i < gridSize; i++)
	{
		grid[i] = new char[gridSize];
		for (int j = 0; j < gridSize; j++)
		{
			grid[i][j] = s[k++];
		}
	}
}

void deleteGridSpace() {
	for (int i = 0; i < gridSize; i++)
	{
		delete[] grid[i];
	}
}

bool check() {
	int k = 0;
	for (int i = 0; i < gridSize; i++)
	{//idx for rows
		for (int j = 0; j < gridSize; j++)
		{//idx for columns
			if (grid[i][j] != s[k])return false;//check for first condition
			if (grid[j][i] != s[k])return false;//check for second condition
			if (grid[gridSize - i - 1][gridSize - j - 1] != s[k])return false;//check for third condition
			if (grid[gridSize - j - 1][gridSize - i - 1] != s[k++])return false;//check for forth condition
		}
	}
	return true;
}

int main() {
	freopen("inp.in", "r", stdin);
	freopen("output", "w", stdout);
	int T;
	scanf("%d%*c", &T);
	for (int t = 1; t <= T; t++) {
		printf("Case #%d:\n", t);
		gets(inp);
		processInput();
		sLen = strlen(s);
		double sqLen = sqrt(sLen*1.0);
		if (sqLen - (int)sqLen > 0) {
			printf("No magic :(\n");
			continue;
		}
		gridSize = (int)sqLen;
		arrangeToGrid();
		if (check())
			printf("%d\n", gridSize);
		else
			printf("No magic :(\n");
		deleteGridSpace();
	}
	return 0;
}
