
#include<stdio.h>

int main()
{
	freopen("inp.in", "r", stdin);
	int T, i, cF, cM;scanf("%d%*c", &T);
	char s[200];
	while (T--)
	{
		gets(s);
		i = -1;
		cF = cM = 0;
		while (s[++i])
		{
			if (s[i] == 'M')cM++;
			else if (s[i] == 'F')cF++;
		}
		if (i > 3 && cF == cM)printf("LOOP\n");
		else printf("NO LOOP\n");
	}
	return 0;
}
