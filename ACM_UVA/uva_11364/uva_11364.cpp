
#include<stdio.h>
int main()
{
	freopen("inp.in", "r", stdin);
	int T;scanf("%d", &T);
	int size, min, max, n;
	while (T--)
	{
		scanf("%d ", &size);
		min = 100;max = 0;
		for (int i = 0; i < size; i++)
		{
			scanf("%d", &n);
			if (min > n)min = n;
			if (max < n)max = n;
		}
		printf("%d\n", 2 * (max - min));
	}
	return 0;
}
