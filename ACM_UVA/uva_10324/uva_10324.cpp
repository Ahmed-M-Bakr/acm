
#include<stdio.h>

int main()
{
	freopen("inp.in", "r", stdin);
	char arr[1000001], c;
	int num, a, b, temp, idx = 0;
	bool test;
	while (scanf("%s", arr) == 1)
	{
		scanf("%d", &num);
		printf("Case %d:\n", ++idx);
		for (int i = 0; i < num; i++)
		{
			scanf("%d %d%*c", &a, &b);
			if (a > b)
			{
				temp = a;
				a = b;
				b = temp;
			}
			c = arr[a];
			test = true;
			for (int i = a + 1; i <= b; i++)
				if (arr[i] != c) { test = false;break; }
			if (test)printf("Yes\n");
			else printf("No\n");
		}
	}
	return 0;
}
