
//https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=1985
#include<stdio.h>
#include<math.h>
int main()
{
	freopen("inp.in", "r", stdin);
	int T, nRows, nCols;scanf("%d", &T);
	while (T--)
	{
		scanf("%d %d", &nRows, &nCols);
		printf("%d\n", (nRows / 3) * (nCols / 3));

	}
	return 0;
}
