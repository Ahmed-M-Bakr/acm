
#include<stdio.h>
#include<string.h>
int main()
{
	freopen("inp.in", "r", stdin);
	int SIZE = 6;
	char *wordArr[] = { "HELLO" , "HOLA" , "HALLO" , "BONJOUR" , "CIAO" , "ZDRAVSTVUJTE" };
	char *countryArr[] = { "ENGLISH" , "SPANISH" , "GERMAN" , "FRENCH" , "ITALIAN" , "RUSSIAN" };
	char inp[80];
	int c = 0;
	while (true)
	{
	l:scanf("%[A-Z#]%*c", &inp);
		if (!strcmp(inp, "#"))break;
		for (int i = 0; i < SIZE; i++)
			if (!strcmp(wordArr[i], inp)) { printf("Case %d: %s\n", ++c, countryArr[i]); goto l; }
		printf("Case %d: UNKNOWN\n", ++c);
	}
	return 0;
}
