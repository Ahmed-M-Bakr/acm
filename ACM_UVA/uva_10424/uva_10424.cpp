
#include<stdio.h>
#include<string.h>

int main()
{
	freopen("inp.in", "r", stdin);
	char n1[50], n2[50];
	int s1, s2, i, j, t1, t2;
	while (true)
	{
		char *m = gets(n1);
		if (m == NULL)break;
		m = gets(n2);
		if (m == NULL)break;
		i = j = s1 = s2 = 0;
		while (n1[i])
		{
			if (n1[i] >= 'a' && n1[i] <= 'z')s1 += (int)(n1[i] - 'a') + 1;
			else if (n1[i] >= 'A' && n1[i] <= 'Z')s1 += (int)(n1[i] - 'A') + 1;
			i++;
		}
		while (s1 > 9)
		{
			t1 = s1;
			s1 = 0;
			while (t1 > 0)
			{
				s1 += t1 % 10;
				t1 /= 10;
			}
		}
		while (n2[j])
		{
			if (n2[j] >= 'a' && n2[j] <= 'z')s2 += (int)(n2[j] - 'a') + 1;
			else if (n2[j] >= 'A' && n2[j] <= 'Z')s2 += (int)(n2[j] - 'A') + 1;
			j++;
		}
		while (s2 > 9)
		{
			t2 = s2;
			s2 = 0;
			while (t2 > 0)
			{
				s2 += t2 % 10;
				t2 /= 10;
			}
		}
		if (s1 < s2)printf("%.2f %\%\n", (s1*1.0 / s2) * 100);
		else printf("%.2f %\%\n", (s2*1.0 / s1) * 100);
	}
	return 0;
}
