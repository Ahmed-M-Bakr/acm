//AC
#include<stdio.h>
int main() {
	freopen("inp.in", "r", stdin);
	long long T, x, y, sum, dif;
	scanf("%llu", &T);

	while (T--) {
		scanf("%llu %llu", &sum, &dif);
		x = (sum + dif) / 2;
		y = sum - x;
		if (x < 0 || y < 0 || ((sum + dif) % 2) != 0)printf("impossible\n");
		else printf("%llu %llu\n", x, y);
	}
	return 0;
}
