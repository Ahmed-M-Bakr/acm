//AC
#include<stdio.h>

bool isPal(unsigned long long n) {
	char arr[10];
	int size = 0;
	while (n) {
		arr[size++] = n % 10;
		n /= 10;
	}

	for (int i = 0; i < size; i++)
	{
		if (arr[i] != arr[size - i - 1])return false;
	}
	return true;
}

int reverseNum(unsigned long long n) {
	unsigned long long res = 0;
	while (n) {
		res *= 10;
		res += n % 10;
		n /= 10;
	}
	return res;
}

void finPal(unsigned long long n) {
	int count = 0;
	do {
		count++;
		n += reverseNum(n);
	} while (!isPal(n));
	printf("%d %llu\n", count, n);
}

int main() {
	freopen("inp.in", "r", stdin);
	int T;
	unsigned long long n;
	scanf("%d", &T);
	while (T--) {
		scanf("%llu", &n);
		finPal(n);
	}
	return 0;
}
