
#include<stdio.h>
#include<string.h>
#include<map>
#include<string>
using namespace std;

int countt;
map<string, int>m;
bool isPalindrom(const char *s, const int len) {
	for (int i = 0; i < len / 2; i++)
		if (s[i] != s[len - i - 1])
			return false;
	//printf("%s\n" , s);
	return true;
}
void findSubstrings(const char * s) {
	char sub[100];
	while (*s) {//chooses the first character in the string
		int j;
		for (j = 0; *(s + j); j++)//j = sub-string-length - 1
		{//the condition ensures that the last character in the substring is not '\0'
			int k;
			for (k = 0; k <= j; k++)
			{
				sub[k] = *(s + k);
			}
			sub[k] = '\0';
			//*************************you can use the sub-string in (sub) here***************
			string ss = string(sub);
			if (m.find(ss) == m.end() && isPalindrom(sub, j + 1)) {
				m[ss] = 0;
				countt++;
			}
			//*****************************************END****************************************************
		}
		s++;
	}
}


int main() {
	freopen("inp.in", "r", stdin);
	char s[100];
	while (gets(s) != NULL) {
		countt = 0;
		m.clear();
		findSubstrings(s);
		printf("The string '%s' contains %d palindromes.\n", s, countt);
	}
	return 0;
}
