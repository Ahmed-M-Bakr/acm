
#include<stdio.h>
int main()
{
	freopen("inp.in", "r", stdin);
	int T, size;scanf("%d%*c", &T);
	char x[80];
	while (T--)
	{
		scanf("%[a-z]%*c", x);
		int size = 0;
		while (x[size++]);
		size--;
		if (size == 5)printf("3\n");
		else if (size == 3 && (x[0] == 'o' || x[1] == 'n'))printf("1\n");
		else printf("2\n");
	}
	return 0;
}
