//AC
#include<stdio.h>

int main() {
	freopen("inp.in", "r", stdin);
	freopen("output", "w", stdout);
	int n, numPages, virtualN, leftSide, rightSide;
	while (1) {
		scanf("%d", &n);
		if (!n)break;
		printf("Printing order for %d pages:\n", n);
		numPages = n / 4;
		if (n % 4 != 0)numPages++;
		virtualN = numPages * 4;
		for (int i = 1; i <= numPages; i++)
		{
			printf("Sheet %d, front: ", i);
			rightSide = ((i - 1) * 2) + 1;
			leftSide = virtualN - rightSide + 1;
			if (leftSide > n)
				printf("Blank, ");
			else
				printf("%d, ", leftSide);
			if (rightSide > n)
				printf("Blank\n");
			else
				printf("%d\n", rightSide);

			leftSide = rightSide + 1;
			rightSide = virtualN - leftSide + 1;
			if (leftSide > n && rightSide > n)continue;
			printf("Sheet %d, back : ", i);
			if (leftSide > n)
				printf("Blank, ");
			else
				printf("%d, ", leftSide);
			if (rightSide > n)
				printf("Blank\n");
			else
				printf("%d\n", rightSide);
		}
	}
	return 0;
}
